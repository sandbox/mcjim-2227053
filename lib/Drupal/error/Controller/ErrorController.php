<?php

namespace Drupal\error\Controller;

use Drupal\Core\Controller\ControllerBase;

class ErrorController extends ControllerBase {

  public function page() {
   $batch = array(
     'title' => t('Error'),
     'operations' => array(
       array('error_batch_callback', array()),
     ),
     'finished' => 'error_batch_finished',
     'file' => drupal_get_path('module', 'error') . '/error_batch.callbacks.inc',
   );
   batch_set($batch);
   return batch_process('admin/config', 'batch');
  }
}