Produces long messages by default on every page.

Add ?short to the URL to produce single-line messages.

Add ?double to the URL to double-up the messages.